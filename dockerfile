FROM node:16-alpine3.16

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000
